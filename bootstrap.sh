#!/usr/bin/env bash

function is_present() 
{
    echo -n checking for $1.....:
    which $1 2>/dev/null
    if [ $? -eq 1 ]; then
	    echo "Not found."
	    echo "Install "$2
	    exit;
    fi;
}

echo LForge bootstrap...
echo

is_present "python3"    
is_present "virtualenv" 
is_present "gcc"        
is_present "cpp"        
is_present "g++"        
is_present "make"       
is_present "bison"
is_present "flex"
is_present "curl"
is_present "md5sum"
is_present "mkfs.ext4"

# Make necessary folders

mkdir -p bin/toolchain
mkdir -p src/toolchain
mkdir buildenv
mkdir log
mkdir -p staging/kernel
mkdir -p staging/initramfs
mkdir -p staging/rootfs

# Kickstarting build env

virtualenv -p python3 buildenv
cd buildenv
source bin/activate

bin/pip3 install inquirer
bin/pip3 install requests
bin/pip3 install beautifulsoup4
bin/pip3 install pyyaml

cd ..

./lforge.py
