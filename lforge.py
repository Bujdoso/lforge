#!buildenv/bin/python3

import os
from os.path import exists

import subprocess

import re
import inquirer

import urllib.request
import urllib.parse

from bs4 import BeautifulSoup

import yaml

emb_root = os.getcwd()
make_cpus = 1

def get_state(module, state):
    main_state = {}
    with open(emb_root + "/.state.yml", "r") as state_file:
        main_state = yaml.safe_load(state_file)
    state_file.close()

    if module in main_state:
        if state in main_state[module]:
            return main_state[module][state]
        else:
            return None
    else:
        if state in main_state[module]['args']:
            return state
        else:
            return None


def is_state(module,  state):
    if not exists(emb_root + "/.state.yml"):
        return False
    main_state = {}
    with open(emb_root + "/.state.yml", "r") as state_file:
        main_state = yaml.safe_load(state_file)
    state_file.close()

    if module in main_state:
       if ('args' in main_state[module]):
           if state in main_state[module]['args']:
                return True
       if state in main_state[module]:
            return True
    return False

def add_state(module, key, val):
    main_state = {}
    with open(emb_root + "/.state.yml", "r") as state_file:
        main_state = yaml.safe_load(state_file)

    if module not in main_state:
        main_state[module] = {}

    if (key == 'name') or \
       (key == 'md5')  or \
       (key == 'defconfig'):
       main_state[module][key] = val;

    if key == 'args':
        if 'args' not in main_state[module]:
            main_state[module]['args'] = []
            main_state[module]['args'].append(val);

    with open(emb_root + "/.state.yml", "w") as state_file:
        state_file.write(yaml.dump(main_state, indent = 4, default_flow_style = False))
    state_file.close()


def uncompress_tool(tool, tool_file, file_type):
    if file_type == 'tool':
        dest_path = 'src/toolchain/'
    else:
        dest_path = 'src/packages/'
    if is_state(tool, 'uncompressed'):
        print(tool + " (" + tool_file + ") is already uncompressed, skipping");
        return

    print("Uncompressing " + tool + " source...");
    command = "tar xf " + dest_path + tool_file + " -C " + dest_path 
    if os.system(command) == 0:
        add_state(tool, 'args', uncompressed)
    else:
        print("Uncompress failed, exiting.")
        exit()

def fetch_gnu_tool(tool, gnu_tool_url, gnu_file, file_type):
    if file_type == 'tool':
        dest_path = 'src/toolchain/'
    else:
        dest_path = 'src/packages/'
    if is_state(tool, 'md5'):
        md5out = subprocess.check_output("/usr/bin/md5sum " + dest_path + gnu_file, shell=True)
        md5 = md5out.decode().split(' ')
        md5_state = get_state(tool,'md5')
        if (md5_state == md5[0]):
            print("You already downloaded " + tool + " and the checksum is valid.")
            return
        else:
            print("Found a partial or damaged download of " + tool + ", re-downloading.")

    print("Fetching " + tool + ".....")
    command = "curl " + gnu_tool_url + " -o " + dest_path + gnu_file

    if os.system(command) == 0:
        md5out = subprocess.check_output("/usr/bin/md5sum " + dest_path + gnu_file, shell=True)
        md5 = md5out.decode().split(' ')
        add_state(tool, 'md5', md5[0])
    else:
        print("Fetch failed, exiting.")
        exit()

def uncompress_kernel():
    if is_state('kernel', 'uncompressed'):
        print("kernel is already uncompressed, skipping");
        return

    kernel_state = get_state('kernel', 'name')
    kernel_path = kernel_state.split("/")
    kernel_file = kernel_path[1];

    print("Uncompressing kernel " + kernel_file + " source...");
    command = "tar xf src/kernel/" + kernel_file + " -C src/kernel/"
    if os.system(command) == 0:
        add_state('kernel', 'args', 'uncompressed')
    else:
        print("Uncompress failed, exiting.")
        exit()

def get_kernel():
    kernel_state = get_state('kernel', 'name')
    kernel_path = kernel_state.split("/")
    kernel_file = kernel_path[1].replace(".tar.gz","");
    return kernel_file

def get_main_arch():
    main_arch_array = arch.split("-")
    main_arch = main_arch_array[0];
    return main_arch

def install_kernel_headers():
    if is_state('kernel','headers'):
        print("Kernel headers are already installed. Skipping.")
        return

    kernel_file = get_kernel()
    main_arch = get_main_arch()

    os.chdir(emb_root + "/src/kernel/" + kernel_file)
    command = "make ARCH=" + main_arch + " INSTALL_HDR_PATH=" + tool_prefix + "/" + arch + " headers_install"

    if os.system(command) > 0:
        os.chdir(emb_root)
        exit()
    add_state('kernel', 'args', 'headers')

def fetch_kernel():
    if is_state('kernel', 'name'):
        kernel_path = get_state('kernel','name').split("/")
        kernel_file = kernel_path[1]

        kernel_local_path = "src/kernel/" + kernel_path[1]
        if exists(kernel_local_path):
            md5out = subprocess.check_output("/usr/bin/md5sum src/kernel/" + kernel_file, shell=True)
       	    md5 = md5out.decode().split(' ')
            md5_state = get_state('kernel','md5')
            if (md5_state == md5[0]):
                print("You already downloaded kernel " + kernel_file + " and the checksum is valid.")
                return
            else:
            	print("Found a partial or damaged download of kernel " + kernel_file + ", re-downloading.")

    print("Fetching kernel " + kernel_file + ".....")
    command = "curl -L https://www.kernel.org/pub/linux/kernel/" + kernel_path[0] + "/" + kernel_path[1] + " -o src/kernel/" + kernel_file
    if os.system(command) == 0:
        md5out = subprocess.check_output("/usr/bin/md5sum src/kernel/" + kernel_file, shell=True)
        md5 = md5out.decode().split(' ')
        add_state('kernel','md5', md5[0])
    else:
        print("Fetch failed, exiting.")
        exit()

def pick_kernel():
    if is_state('kernel', 'name'):
        kernel_file = get_state('kernel','name')
        print("You already selected " + kernel_file + ", skipping")
        return kernel_file

    print("Fetching available kernel major versions...")
    kernel_page = urllib.request.urlopen('https://www.kernel.org/pub/linux/kernel/')
    kernel_page_html = kernel_page.read().decode('utf-8')
    soup = BeautifulSoup(kernel_page_html, 'html.parser');
    kernel_major_versions = soup.find_all('a');

    major_versions = []

    for s in kernel_major_versions:
        major_version = s.extract().get_text()   
        if (major_version.startswith('v')):
            major_versions.append(major_version);
    which_major_version = [
        inquirer.List('answer',
            message="Select major kernel version",
            choices=major_versions,  
        ),
    ]
    selection = inquirer.prompt(which_major_version)
    kernel_major_version = selection["answer"]

    print("Fetching available kernel " + kernel_major_version + " versions...")
    kernel_page = urllib.request.urlopen('https://www.kernel.org/pub/linux/kernel/' + kernel_major_version)
    kernel_page_html = kernel_page.read().decode('utf-8')
    soup = BeautifulSoup(kernel_page_html, 'html.parser');
    kernel_versions = soup.find_all('a'); 
    
    versions = []
    
    for s in kernel_versions:
        version = s.extract().get_text()
        if (version.startswith('linux-')):
            if (version.endswith('.tar.gz')):
                versions.append(version)
    which_version = [
        inquirer.List('answer',
            message="Select kernel version",
            choices=versions,
        ),
    ]
    selection = inquirer.prompt(which_version)
    kernel_version = selection["answer"]

    add_state('kernel', 'name', kernel_major_version + kernel_version);
    return kernel_major_version + kernel_version



def pick_kernel_defconfig():
    if is_state('kernel', 'defconfig'):
        defconfig = get_state('kernel','defconfig')
        print("Kernel defconfig is already set to " + defconfig)
        return

    kernel_file = get_kernel()
    main_arch = get_main_arch()

    print("Fetching defconfigs for architecture " + main_arch)
    defconfig_list = os.listdir(emb_root + "/src/kernel/" + kernel_file + "/arch/" + main_arch + "/configs")
    defconfig_list.sort()
    which_defconfig = [
        inquirer.List('answer',
            message="Select which defconfig you want for your board",
            choices=defconfig_list,
        ),
    ]
    selection = inquirer.prompt(which_defconfig)
    defconfig = selection["answer"]
 
    os.chdir(emb_root + "/src/kernel/" + kernel_file)
    cmd = "PATH=" + tool_prefix + "/bin:$PATH"
    cmd += " ARCH=" + main_arch  
    cmd += " CROSS_COMPILE=" + arch + "-" 
    cmd += " make " + defconfig
    print(cmd)
    if os.system(cmd) > 0:
        os.chdir(emb_root)
        exit()

    add_state('kernel', 'defconfig', defconfig)


def busybox_menuconfig():
    if is_state('busybox', 'menuconfig'):
        print("Busybox menuconfig is already done.")
        return

    question = [
        inquirer.List('menuconfig',
            message="Do you want to run menuconfig before busybox build?",
            choices=["Yes", "No"],
        ),
    ]
    answer = inquirer.prompt(question)
    if answer['menuconfig'] == 'No':
        return

    busybox_file = get_state('busybox', 'name')
    main_arch = get_main_arch()

    os.chdir(emb_root + "/src/packages/" + busybox_file)
    cmd = "PATH=" + tool_prefix + "/bin:$PATH"
    cmd += " ARCH=" + main_arch
    cmd += " CROSS_COMPILE=" + arch + "-"
    cmd += " make menuconfig"
    print(cmd)
    if os.system(cmd) > 0:
        os.chdir(emb_root)
        exit()

    add_state('busybox', 'args', 'menuconfig')


    
def kernel_menuconfig():
    if is_state('kernel', 'menuconfig'):
        print("Kernel menuconfig is already done.")
        return

    question = [
        inquirer.List('menuconfig',
            message="Do you want to run menuconfig before kernel build?",
            choices=["Yes", "No"],
        ),
    ]
    answer = inquirer.prompt(question)

    kernel_file = get_kernel()
    main_arch = get_main_arch()

    os.chdir(emb_root + "/src/kernel/" + kernel_file)
    cmd = "PATH=" + tool_prefix + "/bin:$PATH"
    cmd += " ARCH=" + main_arch
    cmd += " CROSS_COMPILE=" + arch + "-"
    cmd += " make menuconfig"
    print(cmd)
    if os.system(cmd) > 0:
        os.chdir(emb_root)
        exit()

    add_state('kernel', 'args', 'menuconfig')


def busybox_build():
    if is_state('busybox' , 'built'):
        print("Busybox is already built.")
        return

    busybox_file = get_state('busybox', 'name')
    main_arch = get_main_arch()

    os.chdir(emb_root + "/src/packages/" + busybox_file)
    cmd = "PATH=" + tool_prefix + "/bin:$PATH"
    cmd += " ARCH=" + main_arch
    cmd += " CROSS_COMPILE=" + arch + "-"
    cmd += " make -j" + make_cpus
    print(cmd)
    if os.system(cmd) > 0:
        os.chdir(emb_root)
        exit()
    add_state('busybox', 'args', 'built')


def busybox_install():
    if is_state('busybox', 'installed'):
        print("Busybox is already installed.")
        return

    busybox_file = get_state('busybox','name')
    main_arch = get_main_arch()

    os.chdir(emb_root + "/src/packages/" + busybox_file)
    cmd = "PATH=" + tool_prefix + "/bin:$PATH"
    cmd += " make -j" + make_cpus
    cmd += " ARCH=" + main_arch
    cmd += " CROSS_COMPILE=" + arch + "-"
    cmd += " CONFIG_PREFIX=" + emb_root + "/staging/initramfs"
    cmd += " install" 
    print(cmd)
    if os.system(cmd) > 0:
        os.chdir(emb_root)
        exit()
    add_state('busybox', 'args', 'installed')


def initramfs_build():
    if is_state('initramfs', 'built'):
        print("Initramfs is already built. Skipping.")
        return
    os.chdir(emb_root + "/staging/initramfs")
    os.system("mkdir -pv dev etc mnt/root proc root sys")
    os.system("cp " + emb_root + "/scripts/init .")
    os.system("chmod 755 init")
    os.system("find . -print0 | cpio --null -ov --format=newc > ../initramfs.cpio")
    print("Initramfs was built using the init script in the scripts folder.")
    os.chdir(emb_root)
    add_state('initramfs', 'args', 'built')


def kernel_build():
    if is_state('kernel', 'built'):
        print("Kernel is already built.")
        return

    kernel_file = get_kernel()
    main_arch = get_main_arch()

    os.chdir(emb_root + "/src/kernel/" + kernel_file)
    cmd = "PATH=" + tool_prefix + "/bin:$PATH"
    cmd += " ARCH=" + main_arch
    cmd += " CROSS_COMPILE=" + arch + "-"
    cmd += " make -j" + make_cpus
    cmd += " zImage"
    print(cmd)
    if os.system(cmd) > 0:
        os.chdir(emb_root)
        exit()
    os.system("cp " + emb_root + "/src/kernel/" + kernel_file + "/arch/" + main_arch + "/boot/zImage " + emb_root + "/staging/kernel/.")
    os.system("cp " + emb_root + "/src/kernel/" + kernel_file + "/arch/" + main_arch + "/boot/Image " + emb_root + "/staging/kernel/.")
    print("Kernel is copied into staging/kernel/" + kernel_file + " - both as vmlinux and zImage.")
    print("Misc specific files are still in src/kernel/" + kernel_file)
    add_state('kernel', 'args','built')



def kernel_modules_build():
    if is_state('kernel', 'modules'):
        print("Kernel modules are already built.")
        return

    kernel_file = get_kernel()
    main_arch = get_main_arch()

    os.chdir(emb_root + "/src/kernel/" + kernel_file)
    cmd = "PATH=" + tool_prefix + "/bin:$PATH"
    cmd += " ARCH=" + main_arch
    cmd += " CROSS_COMPILE=" + arch + "-"
    cmd += " make -j" + make_cpus
    cmd += " INSTALL_MOD_PATH=" + emb_root + "/staging/kernel"
    cmd += " modules_install"
    print(cmd)
    if os.system(cmd) > 0:
        os.chdir(emb_root)
        exit()
    add_state('kernel','args','modules')



def pick_gnu_tool(url, tool, ext):
    if is_state(tool, 'name'):
        gnu_file = get_state(tool, 'name')
        print("You already selected " + tool + ", skipping")
        return gnu_file

    print("Fetching available " + tool + " versions...")

    gnu_page = urllib.request.urlopen(url)
    gnu_page_html = gnu_page.read().decode('utf-8')

    soup = BeautifulSoup(gnu_page_html, 'html.parser');

    gnu_versions = soup.find_all('a');

    versions = []

    for s in gnu_versions:
        version = s.extract().get_text()
        if (version.find(tool + '-')) >= 0:
            if (version.endswith(ext)):
                versions.append(version.replace(ext,""));

    which_tool = [
        inquirer.List('answer',
            message="Select your target " + tool,
            choices=versions,
        ),
    ]

    selection = inquirer.prompt(which_tool)
    gnu_file = selection["answer"]
    add_state(tool, 'name', gnu_file)
    return gnu_file


def pick_arch():
    print("Welcome to Linux Forge. Let's set up the prerequisities.")

    if is_state('arch', 'name'):
        print("You already picked an architecture, skipping.")
        return get_state('arch', 'name')

    which_arch = [
        inquirer.List('answer',
            message="Select your target architecture",
            choices=['arm-linux-gnueabi',
                     'aarch64-linux-gnu',
                     'mips-linux-gnu',
                     'mips64-linux-gnu',
                     'riscv-linux-gnu',
                     'i386-linux-gnu',
                     'x86_64-linux-gnu']
        ),
    ]

    selection = inquirer.prompt(which_arch)
    arch =  selection["answer"]
    
    main_arch = get_main_arch()
    if main_arch == 'arm':
        march='--with-march=arm7-a --with-float=hard';
    if main_arch == 'mips':
        march=''
    if main_arch == 'aarch64':
        march=''
    if main_arch == 'mips64':
        march=''
    if main_arch == 'riscv':
        march=''


    add_state('arch', 'name', arch)
    return arch


def pick_make_cpus():
    if is_state('make','cpus'):
        set_cpus = get_state('make', 'cpus') 
        print("You already picked " + set_cpus + " cores to build on, continuing with that.")
        return set_cpus
    cpuscmd = subprocess.check_output("/usr/bin/nproc", shell=True)
    cpus = cpuscmd.decode().replace("\n","");

    question = [
      inquirer.Text('select_cpus', message="Found " + cpus + " cpus. How many should be used for building")
    ]
    answer = inquirer.prompt(question)
    picked_cpus = answer['select_cpus']
    add_state('make', 'cpus', picked_cpus)
    return picked_cpus

def binutils_configure():
    if (is_state("binutils', 'configured")):
        print("Looks like binutils is already configured. Skipping.")
    else:
        print("Configuring binutils for " + arch);
        os.chdir(emb_root + "/src/toolchain/" + binutils);
        conf_cmd = "./configure"
        conf_cmd += " --prefix=" + tool_prefix 
        conf_cmd += " --target=" + arch 
        conf_cmd += " " + march
        conf_cmd += " --disable-multilib"
        conf_cmd += " --disable-werror"
        if (os.system(conf_cmd)) > 0:
            os.chdir(emb_root);
            exit()
        add_state('binutils', 'args', 'configured');


def binutils_build():
    if (is_state('binutils', 'built')):
        print("Looks like binutils is already built. Skipping.")
    else:
        print("Building binutils")
        if (os.system("make -j" + make_cpus)) > 0:
            os.chdir(emb_root);
            exit()
        print("Binutils is built.")
        add_state('binutils', 'args', 'built')


def binutils_install():
    if (is_state('binutils', 'installed')):
        print("Looks like binutils is already installed. Skipping.")
    else:
        if (os.system("make install")) > 0:
            os.chdir(emb_root);
            exit()
        print("Binutils is installed.")
        add_state('binutils', 'args' ,'installed')
        os.chdir(emb_root);


def gcc_configure():
    gcc = get_state('gcc', 'name');
    if (is_state('gcc', 'configured')):
        print("Looks like gcc is already configured. Skipping.")
    else:
        os.chdir(emb_root + "/src/toolchain/" + gcc);

        if (os.system("./contrib/download_prerequisites") > 0):
            exit()

        os.mkdir("build")
        os.chdir("build")

        print("Configuring gcc for " + arch);

        conf_cmd  = "../configure "
        conf_cmd += " --prefix=" + tool_prefix
        conf_cmd += " --target=" + arch 
        conf_cmd += " " + march
        conf_cmd += " --disable-threads"
        conf_cmd += " --disable-multilib"
        conf_cmd += " --disable-werror" 
        conf_cmd += " --enable-languages=c,c++"
        if (os.system(conf_cmd)) > 0:
            os.chdir(emb_root);
            exit()
        add_state('gcc' ,'args', 'configured');
        os.chdir(emb_root);


def gcc_build():
    if (is_state('gcc', 'built')):
        print("Looks like gcc is already built. Skipping.")
    else:
        print("Building gcc")
        os.chdir(emb_root + "/src/toolchain/" + gcc + "/build")
        if (os.system("make -j" + make_cpus + " all-gcc")) > 0:
            os.chdir(emb_root);
            exit()
        os.chdir(emb_root);
        print("gcc is built.")
        add_state('gcc', 'args', 'built')


def libgcc_build():
    if not is_state('gcc', 'built'):
        print("gcc needs to be built first. Exiting.")
        os_chdir(emb_root)
        exit()
    if not is_state('glibc', 'built'):
        print("libc needs to be built first. Exiting.")
        os_chdir(emb_root)
        exit()
    if (is_state('libgcc', 'built')):
        print("Looks like libgcc is already built. Skipping.")
    else:
        print("Building libgcc.");
        os.chdir(emb_root + "/src/toolchain/" + gcc + "/build")
        if (os.system("make -j" + make_cpus + " all-target-libgcc")) > 0:
            os.chdir(emb_root);
            exit()
        os.chdir(emb_root);
        print("libgcc is built.")
        add_state('libgcc', 'args', 'built')

def libstdc_build():
    if not is_state('gcc','built'):
        print("gcc needs to be built first. Exiting.")
        os_chdir(emb_root)
        exit()
    if not is_state('glibc','built'):
        print("libc needs to be built first. Exiting.")
        os_chdir(emb_root)
        exit()
    if not is_state('libgcc','built'):
        print("libgcc needs to be built first. Exiting.")
        os_chdir(emb_root)
        exit()        
    if (is_state('libstdc','built')):
        print("Looks like libstdc is already built. Skipping.")
    else:
        print("Building libstdc.");
        os.chdir(emb_root + "/src/toolchain/" + gcc + "/build")
        if (os.system("make -j" + make_cpus)) > 0:
            os.chdir(emb_root);
            exit()
        os.chdir(emb_root);
        print("libgcc is built.")
        add_state('libstdc','args','built')



def gcc_install():
    if (is_state('gcc','installed')):
        print("Looks like gcc is already installed. Skipping.")
    else:
        os.chdir(emb_root + "/src/toolchain/" + gcc + "/build")
        if (os.system("make install-gcc")) > 0:
            os.chdir(emb_root);
            exit()
        print("Gcc is installed.")
        add_state('gcc','args','installed')
        os.chdir(emb_root);


def libgcc_install():
    if (is_state('libgcc','installed')):
        print("Looks like libgcc is already installed. Skipping.")
    else:
        print("Installing libgcc.")
        os.chdir(emb_root + "/src/toolchain/" + gcc + "/build")
        if (os.system("make install-target-libgcc")) > 0:
            os.chdir(emb_root);
            exit()
        print('libgcc is installed.')
        add_state('libgcc','args','installed')
        os.chdir(emb_root);


def libstdc_install():
    if (is_state('libstdc','installed')):
        print("Looks like libstdc is already installed. Skipping.")
    else:
        print("Installing libstdc.")
        os.chdir(emb_root + "/src/toolchain/" + gcc + "/build")
        if (os.system("make install")) > 0:
            os.chdir(emb_root);
            exit()
        print("libstdc is installed.")
        add_state('libstdc','args','installed')
        os.chdir(emb_root);


def glibc_configure():
    gcc = get_state('glibc', 'name');
    if (is_state('glibc','configured')):
        print("Looks like glibc is already configured. Skipping.")
        # it may be configured but it still can be built, descend into the build dir
    else:
        os.chdir(emb_root + "/src/toolchain/" + glibc);

        if not exists("build"):
            os.mkdir("build")
        os.chdir("build")

        kernel_state = get_state('kernel','name')
        kernel_path = kernel_state.split("/")
        kernel_arc = kernel_path[1].split(".")
        kernel = kernel_arc[0];


        print("Configuring glibc for " + arch);
        conf_cmd = "PATH=" + tool_prefix + "/bin:$PATH"
        conf_cmd += " ../configure "
        conf_cmd += arch  
        conf_cmd += " --prefix=" + tool_prefix + "/" + arch
        conf_cmd += " --target=" + arch 
        conf_cmd += " --host=" + arch 
        conf_cmd += " --build=" + machtype
        conf_cmd += " --with-arch=armv7-a"
        conf_cmd += " --with-float=soft" 
        conf_cmd += " --with-headers=" + tool_prefix + "/" + arch + "/include"
        conf_cmd += " --disable-multilib"
        conf_cmd += " libc_cv_forced_unwind=yes"
        print(conf_cmd)
        if (os.system(conf_cmd)) > 0:
            os.chdir(emb_root);
            exit()
        add_state('glibc','args','configured');
        os.chdir(emb_root);



def glibc_build():
    if (is_state('glibc','built')):
        print("Looks like glibc is already built. Skipping.")
    else:
        print("Building glibc.");
        os.chdir(emb_root + "/src/toolchain/" + glibc + "/build")
        make_cmd  = "PATH=" + tool_prefix + "/bin:$PATH "
        make_cmd += "make install-bootstrap-headers=yes install-headers"
        if (os.system(make_cmd)) > 0:
            os.chdir(emb_root)
            exit()
        make_cmd  = "PATH=" + tool_prefix + "/bin:$PATH"
        make_cmd += " make -j" + make_cpus
        make_cmd += " csu/subdir_lib"
        if (os.system(make_cmd)) > 0:
            os.chdir(emb_root)
            exit()

        cmd  = "PATH=" + tool_prefix + "/bin:$PATH"
        cmd += " install csu/crt6.o csu/crti.o csu/crtn.o " + tool_prefix + "/" + arch + "/lib"
        if (os.system(cmd)) > 0:
            os.chdir(emb_root)
            exit()

        cmd  = "PATH=" + tool_prefix + "/bin:$PATH "
        cmd += arch + "-gcc"
        cmd += " -nostdlib"
        cmd += " -nostartfiles"
        cmd += " -shared"
        cmd += " -x c /dev/null"
        cmd += " -o " + tool_prefix + "/" +  arch + "/lib/libc.so"
        if (os.system(cmd)) > 0:    
            os.chdir(emb_root)
            exit()
        os.chdir(emb_root)

        cmd = "touch " + tool_prefix + "/" + arch  + "/include/gnu/stubs.h"
        os.system(cmd);

        print("glibc is built.")
        add_state('glibc','args','built')



def glibc_install():
    if (is_state('glibc','installed')):
        print("Looks like glibc is already installed. Skipping.")
    else:
        print("Installing glibc")
        os.chdir(emb_root + "/src/toolchain/" + glibc + "/build")
        cmd  = "PATH=" + tool_prefix + "/bin:$PATH"
        cmd += " make -j" + make_cpus 
        if (os.system(cmd)) > 0:
            os.chdir(emb_root);
            exit()
        cmd  = "PATH=" + tool_prefix + "/bin:$PATH"
        cmd += " make install"
        if (os.system(cmd)) > 0:
            os.chdir(emb_root);
            exit()            
        print("glibc is installed.")
        add_state('glibc','args','installed')
        

def glibc_install_staging():
    if is_state('glibc','staging'):
        print("Looks like glibc is already installed into staging rootfs. Skipping.")
    else:
        print("Install glibc into staging area.")
        os.chdir(emb_root + "/staging/rootfs")
        os.system("mkdir -pv bin lib dev etc mnt/root proc root sbin sys")
        os.system("cp -r " + tool_prefix + "/" + arch + "/lib " + emb_root + "/staging/rootfs/.")
        printf("You should copy the libs into " + emb_root + " /staging/initramfs too if you haven't compiled busybox as a static.")
        add_state('glibc','args','staging')



def debian_package_install_select():
    if is_state('packages','debian'):
        print("Debian package installer was already ran.")
        return

    question = [
        inquirer.List('package_target',
            message="Where do you want to install packages?",
            choices=["initramfs", "rootfs"],
        ),
    ]
    answer = inquirer.prompt(question)
    package_target = answer['package_target']

    if package_target == 'rootfs':
        question = [
            inquirer.List('debian_target',
                message="Do you want to use binary packages from Debian?",
                choices=["Yes", "No"],
            ),
        ]
        answer = inquirer.prompt(question)
        debian_target = answer['debian_target']
        if debian_target == "Yes":
            question = [
                inquirer.Checkbox('packages',
                    message="Do you want to customize the minbase package list?",
                    choices=["kmod",
                             "locales",
                              "udev",
                              "aptitude",
                              "dialog",
                              "ifupdown",
                              "procps",
                              "iproute2",
                              "iputils-ping",
                              "isc-dhcp-client",
                              "nano",
                              "wget",
                              "netbase",
                              "sysvinit-core"]
                ),
            ]
            answer = inquirer.prompt(question)
            debian_bootstrap_packages = answer['packages']
            main_arch = get_main_arch()

            # Map arch to the Debian names
            if main_arch == "arm":
                deb_arch = "armel"
            if main_arch == "mips64":
                deb_arch = "mips64el"
            if main_arch == "x86_64":
                deb_arch = "amd64"

            debootstrap_cmd  = "sudo /usr/sbin/debootstrap "
            debootstrap_cmd += " --verbose --foreign "
            debootstrap_cmd += " --arch=" + deb_arch
            debootstrap_cmd += " --variant=minbase "
            debootstrap_cmd += " --include="
            count = 0
            for package in debian_bootstrap_packages:
                if package is not debian_bootstrap_packages[-1]:
                    debootstrap_cmd += package + ","
                else:
                    debootstrap_cmd += package

            debootstrap_cmd += " buster "+ emb_root + "/staging/rootfs http://ftp.debian.org/debian"
            print("deboostrap will need sudo rights.")
            print(debootstrap_cmd)
            if os.system(debootstrap_cmd):
                exit()


    print("First stage of Debian debootstrap is done.")
    print("You will need to run second stage from within the system itself:")
    print("./debootstrap/debootstrap --second-stage")
    print("apt-get clean")
    print("dpkg-reconfigure tzdata")
    print("dpkg-reconfigure locales")
    add_state('packages','args','debian')


def create_disk_image():
    if is_state('disk_image','created'):
        print("Disk image was already created.")
        return

    os.chdir(emb_root + "/staging")

    print("Disk image creation will need sudo rights.")
    cmd = "dd if=/dev/zero of=disk.img bs=1 count=0 seek=10240M"
    if os.system(cmd):
        print("Disk image creation failed.")
        os.chdir(emb_root)
        return

    cmd="/sbin/mkfs.ext4 -F disk.img"
    if os.system(cmd):
        print("Filesystem creation failed on disk image.")
        os.chdir(emb_root)
        return

    cmd="sudo mount -o loop -t ext4 disk.img /mnt"
    if os.system(cmd):
        print("Disk image mount failed.")
        os.chdir(emb_root)
        return

    cmd="sudo cp -a rootfs/* /mnt/."
    if os.system(cmd):
        print("Couldn't copy rootfs to disk image.")

    cmd="sudo umount /mnt"
    if os.system(cmd):
        print("Couldn't unmount disk image.")
        os.chdir(emb_root)
        return

    add_state('disk_image','args','created')


# -----------------------------------------------------------------------
emb_root = os.getcwd()

machtype = "x86_64-pc-linux-gnu"

arch = pick_arch()

gnu_url = 'http://ftp.gnu.org/gnu/'

gcc = pick_gnu_tool('http://ftp.gnu.org/gnu/gcc/','gcc', '/')
binutils = pick_gnu_tool('http://ftp.gnu.org/gnu/binutils/','binutils','.tar.bz2')
kernel = pick_kernel()

glibc = pick_gnu_tool('http://ftp.gnu.org/gnu/glibc/','glibc','.tar.gz')

busybox = pick_gnu_tool('https://busybox.net/downloads/','busybox','.tar.bz2')

gcc_file = gcc + ".tar.gz"
binutils_file = binutils + ".tar.bz2";
glibc_file = glibc + ".tar.gz";

busybox_file = busybox + ".tar.bz2"

gcc_url = gnu_url + "gcc/" + gcc + "/" + gcc_file
binutils_url = gnu_url + "binutils/" + binutils_file
glibc_url = gnu_url + "libc/" + glibc_file

busybox_url = 'https://busybox.net/downloads/' + busybox_file

fetch_gnu_tool('gcc', gcc_url, gcc_file, 'tool')
fetch_gnu_tool('binutilts', binutils_url, binutils_file, 'tool')
fetch_gnu_tool('glibc', glibc_url, glibc_file, 'tool')

fetch_gnu_tool('busybox', busybox_url, busybox_file, 'package')

fetch_kernel()

uncompress_tool('gcc', gcc_file, 'tool');
uncompress_tool('binutils', binutils_file, 'tool')

uncompress_tool('busybox', busybox_file, 'package')

uncompress_kernel();

make_cpus = pick_make_cpus()
tool_prefix = emb_root + "/bin/toolchain/" 

binutils_configure()
binutils_build()
binutils_install()

install_kernel_headers()

gcc_configure()
gcc_build()
gcc_install()

glibc_configure()
glibc_build()

libgcc_build()
libgcc_install()

glibc_install()

libstdc_build()
libstdc_install()

pick_kernel_defconfig()
kernel_menuconfig()

kernel_build()
kernel_modules_build()

busybox_menuconfig()
busybox_build()
busybox_install()

initramfs_build()

glibc_install_staging()

debian_package_install_select()

create_disk_image()
