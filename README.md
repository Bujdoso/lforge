# LForge

LForge is a tool to build embedded Linux distros. 


## Description

This is a tool which automates the selection, cross-compilation and assembly of gcc, binutils, kernel, libc, busybox.
It supports several platforms and generates all the files necessary to boot up a board.

It is implemented in Python 3 and uses virtualenv to set up a separate environment.


## Installation

Clone the repository then run bootstrap.sh to initialize the environment. It will run lforge.py after all requirements are met.


## Usage

After running embedder.py the tool will go through specific steps to build a cross-toolchain, kernel and busybox.
If a step fails it can be restarted just by running embedder.py again after the issue is fixed.

The tool offers a list of available version of each component it downloads and builds. It's up to your judgement to pick the right versions which work together since it does not make any assumptions.

It offers to run menuconfig for the kernel and busybox.

It supports building initramfs from the built busybox, an init script and optionally libc.

It also offers to build separate packages from source or to populate the rootfs with debootstrap, effectively turning it into an embedded Debian system.

The final result is a kernel, initramfs and a rootfs folder built for the selected architecture and board defconfig.


Depending on which route you choose you may run the sytem from initramfs, boot straight into rootfs or boot initramfs and switch root to rootfs.


## Roadmap

- Support building packages from source
- Support different compile flags and optimizations
- Support for Linux rt-patch
- 


## History

I made the tool for myself to automate building simple embedded distros. It is very simple and limited compared to solutions like Buildroot and Yocto Linux but needed something to build a system from scratch as quickly as possible.  



## License

GNU General Public License version 2
